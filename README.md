TEST TASK 

=======
- Create two applications using Symfony3 framework:
1. server with API entries
2. client

- Server stores users and groups in MySQL database.
user table: id, name, email
group table: id, name

- Client accessing the server through the API:
1. should be able to add, edit, delete users and groups on the server (well, CRUD)
2. should be able to get report with the list of users of each group.

The ready made project should be able to start using Vagrant. 
Assessment will be based on both code and project running within Vagrant.


PALLADA   CLIENT

Commands   (command name    &&    example input argument)

get-users
get-user {user_id}                                                 (just int)
create-user           name=bill email=bill@mail.com role=1         (all arguments required)
update-user           id=11 name=bill email=bill@mail.com role=1   (id-required only)
rm-user  {user_id}                                                 (just int)
get-user-role {role_id}                                            (just int)

get-roles
get-role {role_id}                                                 (just int)
create-role             name=supermoder
update-role             id=20 name=newuser     
rm-role {role_id}                                                   (just int)




UserController

get Users             /users
get one user          /users/{user_id}
create user           /userpost
edit user             /userput/{user_id}
remove user           /usersdel/{user_id}
get same role users   /users/roles/{role_id}

RoleController

get Roles             /roles
get Role              /roles/{role_id}
create Role           /rolepost
edit Role             /roleput{role_id}
remove Role           /roledel/{role_id}
