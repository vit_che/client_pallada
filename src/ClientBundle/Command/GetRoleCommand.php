<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class GetRoleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('get-role')
            ->setDescription('Get Role')
            ->addArgument('id', InputArgument::REQUIRED, 'Role id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        if ( is_int((int)$id) && (int)$id != 0 ) {

            $api_uri = $this->getContainer()->getParameter('api_uri');
            $uri = 'roles/'.$id;
            $client = new Client([ 'base_uri' => $api_uri ]);
            $response = $client->request('GET', $uri);
            $content = $response->getBody()->getContents();
            $role = json_decode($content, true);

            if ($role){
                $output->writeln('Role ID: '.$role['id']);
                $output->writeln('Role name: '.$role['name']);
            } else {
                $output->writeln('Error!');
            }

        } else {
            $output->writeln('Incorrect argument');
        }
    }

}
