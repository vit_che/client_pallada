<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class GetRolesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('get-roles')
            ->setDescription('Get Roles List')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api_uri = $this->getContainer()->getParameter('api_uri');
        $uri = 'roles';
        $client = new Client([
            'base_uri' => $api_uri
        ]);
        $response = $client->request('GET', $uri);
        $content = $response->getBody()->getContents();
        $roles = json_decode($content, true);

        if (count($roles) > 0) {
            $output->writeln('   R O L E S   L I S T');
            foreach ($roles as $role) {
                if ($role) {
                    $output->writeln('==================');
                    $output->writeln('Role ID: ' . $role['id']);
                    $output->writeln('Role name: ' . $role['name']);
                } else {
                    $output->writeln('Error!');
                }
            }
        }

    }
}
