<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class GetUsersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('get-users')
            ->setDescription('Get Users List')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $api_uri = $this->getContainer()->getParameter('api_uri');
        $uri = 'users';
        $client = new Client([
            'base_uri' => $api_uri
        ]);
        $response = $client->request('GET', $uri);
        $content = $response->getBody()->getContents();
        $users = json_decode($content, true);

        if (count($users) > 0){
            $output->writeln('   U S E R S    L I S T');
            foreach($users as $user){
                if ($user){
                    $output->writeln('==================');
                    $output->writeln('User ID: '.$user['id']);
                    $output->writeln('User name: '.$user['name']);
                    $output->writeln('User email: '.$user['email']);
                    $output->writeln('User role: '.$user['role']['name']);
                } else {
                    $output->writeln('Error!');
                }
            }
        }
    }
};
