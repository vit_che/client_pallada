<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;


class UpdateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('update-user')
            ->setDescription('Update user')
            ->addArgument('user', InputArgument::IS_ARRAY, 'User')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = $input->getArgument('user');
        $user_arr = [];
        foreach($user as $elem){
            $num = stripos($elem, '=');
            $key = substr($elem, 0, $num);
            $value = substr(mb_stristr($elem, '='), 1);
            $user_arr += [ $key => $value ];
        }

        $api_uri = $this->getContainer()->getParameter('api_uri');
        $uri = 'users/'.$user_arr['id'];
        $data = [ 'json' =>  $user_arr];
        $client = new Client([ 'base_uri' => $api_uri ]);
        $response = $client->request('PUT', $uri, $data);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);
        if ($message){
            $output->writeln($message['message']);
        } else {
            $output->writeln('No message');
        }
    }
}
