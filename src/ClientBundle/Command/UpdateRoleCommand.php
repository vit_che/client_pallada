<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class UpdateRoleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('update-role')
            ->setDescription('Update Role')
            ->addArgument('role', InputArgument::IS_ARRAY, 'Role')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $role = $input->getArgument('role');
        $role_arr = [];
        foreach($role as $elem){
            $num = stripos($elem, '=');
            $key = substr($elem, 0, $num);
            $value = substr(mb_stristr($elem, '='), 1);
            $role_arr += [ $key => $value ];
        }

        $api_uri = $this->getContainer()->getParameter('api_uri');
        $uri = 'roles/'.$role_arr['id'];
        $data = [ 'json' =>  $role_arr];
        $client = new Client([ 'base_uri' => $api_uri ]);
        $response = $client->request('PUT', $uri, $data);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);

        if ($message){
            $output->writeln($message['message']);
        } else {
            $output->writeln('No message');
        }
    }

}
