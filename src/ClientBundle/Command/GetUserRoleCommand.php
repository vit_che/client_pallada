<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;


class GetUserRoleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('get-user-role')
            ->setDescription('Get User Role ')
            ->addArgument('id', InputArgument::REQUIRED, 'Role ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        if ( is_int((int)$id) && (int)$id != 0 ) {

            $api_uri = $this->getContainer()->getParameter('api_uri');
            $uri = 'users/roles/'.$id;
            $client = new Client([ 'base_uri' => $api_uri ]);
            $response = $client->request('GET', $uri);
            $content = $response->getBody()->getContents();
            $list = json_decode($content, true);

            if ($list){

                $output->writeln($list[0]['role']['name'].'  USERS LIST');
                $output->writeln('=====================');

                foreach($list as $elem){
                    $output->writeln('User ID: '.$elem['id']);
                    $output->writeln('User name: '.$elem['name']);
                    $output->writeln('User email: '.$elem['email']);
                    $output->writeln('User role: '.$elem['role']['name']);
                    $output->writeln('*************');
                }
            } else {
                $output->writeln('Error!');
            }
        }
    }

}
