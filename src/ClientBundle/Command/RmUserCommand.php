<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class RmUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rm-user')
            ->setDescription('...')
            ->addArgument('id', InputArgument::REQUIRED, 'User Id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        if ( is_int((int)$id) && (int)$id != 0 ) {

            $api_uri = $this->getContainer()->getParameter('api_uri');
            $uri = 'users/'.$id;
            $client = new Client([
                'base_uri' => $api_uri
            ]);
            $response = $client->request('DELETE', $uri);
            $content = $response->getBody()->getContents();
            $message = json_decode($content, true);

            if ($message){
                $output->writeln($message['message']);
            } else {
                $output->writeln('No message');
            }

        } else {
            $output->writeln('Incorrect argument');
        }
    }
}
