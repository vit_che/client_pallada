<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class CreateRoleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('create-role')
            ->setDescription('Create Role')
            ->addArgument('name', InputArgument::REQUIRED, 'Role Name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = substr(mb_stristr($input->getArgument('name'), '='), 1);

        $api_uri = $this->getContainer()->getParameter('api_uri');
        $uri = 'roles';
        $data = [ 'json' => [
            "name" => $name,
        ]];
        $client = new Client([ 'base_uri' => $api_uri ]);
        $response = $client->request('POST', $uri, $data);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);
        if ($message){
            $output->writeln($message['message']);
        } else {
            $output->writeln('No message');
        }
    }

}
