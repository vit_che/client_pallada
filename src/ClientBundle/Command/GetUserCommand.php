<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class GetUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('get-user')
            ->setDescription('Get User parameters')
            ->addArgument('id', InputArgument::REQUIRED, 'User id?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');

        if ( is_int((int)$id) && (int)$id != 0 ) {

            $api_uri = $this->getContainer()->getParameter('api_uri');
            $uri = 'users/'.$id;
            $client = new Client([ 'base_uri' => $api_uri ]);
            $response = $client->request('GET', $uri);
            $content = $response->getBody()->getContents();
            $user = json_decode($content, true);

            if ($user){
                $output->writeln('User ID: '.$user['id']);
                $output->writeln('User name: '.$user['name']);
                $output->writeln('User email: '.$user['email']);
                $output->writeln('User role: '.$user['role']['name']);
            } else {
                $output->writeln('Error!');
            }

        } else {
            $output->writeln('Incorrect argument');
        }
    }
}
