<?php

namespace ClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;


class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('create-user')
            ->setDescription('Create new user')
            ->addArgument('name', InputArgument::REQUIRED, 'User Name')
            ->addArgument('email', InputArgument::REQUIRED, 'User Email')
            ->addArgument('role', InputArgument::REQUIRED, 'User Role')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = substr(mb_stristr($input->getArgument('name'), '='), 1);
        $email = substr(mb_stristr($input->getArgument('email'), '='), 1);
        $role = substr(mb_stristr($input->getArgument('role'), '='), 1);

        $api_uri = $this->getContainer()->getParameter('api_uri');
        $uri = 'users';
        $data = [ 'json' => [
            "name" => $name,
            'email' => $email,
            'role' => $role
        ]];
        $client = new Client([ 'base_uri' => $api_uri ]);
        $response = $client->request('POST', $uri, $data);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);
        if ($message){
            $output->writeln($message['message']);
        } else {
            $output->writeln('No message');
        }
    }

}
