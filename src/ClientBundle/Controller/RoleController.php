<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use GuzzleHttp\Client;

class RoleController extends Controller
{
    /**
     * @Route("/roles", name="roles")
     *
     * Get roles list
     */
    public function getAllRolesAction()
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'roles';
        $client = new Client(['base_uri' => $api_uri]);
        $responce = $client->request('GET', $uri);
        $content = $responce->getBody()->getContents();
        $roles = json_decode($content, true);

        return $this->json($roles);
    }

    /**
     * @Route("/roles/{id}", name="role")
     *
     * Get one role
     */
    public function getOneRoleAction($id)
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'roles/'.$id;
        $client = new Client(['base_uri' => $api_uri]);
        $responce = $client->request('GET', $uri);
        $content = $responce->getBody()->getContents();
        $role = json_decode($content, true);

        return $this->json($role);
    }

    /**
     * @Route("/rolepost", name="rolepost")
     *
     * Create new role
     */
    public function postRoleAction()
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'roles';
        $data = [ 'json' => [ "name" => "qwertys" ]];
        $client = new Client(['base_uri' => $api_uri]);
        $response = $client->request('POST', $uri, $data );
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);

        return $this->json($message);
    }

    /**
     * @Route("/roleput/{id}", name="roleput")
     *
     * Edit role
     */
    public function editOneRoleAction($id)
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'roles/'.$id;
        $data = [ 'json' => [ "name" => "qwery" ]];
        $client = new Client(['base_uri' => $api_uri]);
        $response = $client->request('PUT', $uri, $data);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);

        return $this->json($message);
    }

    /**
     * @Route("/roledel/{id}", name="roledel")
     *
     * Remove role
     */
    public function deleteOneRoleAction($id)
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'roles/'.$id;
        $client = new Client(['base_uri' => $api_uri]);;
        $response = $client->request('DELETE', $uri);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);

        return $this->json($message);
    }
}
