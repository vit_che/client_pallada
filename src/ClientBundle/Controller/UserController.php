<?php

namespace ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\Response;

class UserController extends Controller
{
    /**
     * @Route("/users", name="users")
     *
     * Get All users List
     */
    public function getAllUsersAction()
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'users';
        $client = new Client([
            'base_uri' => $api_uri
        ]);
        $response = $client->request('GET', $uri);
        $content = $response->getBody()->getContents();
        $users = json_decode($content, true);

        return $this->json($users);
    }


    /**
     * @Route("/users/{id}", name="user")
     *
     * Get ONE user
     */
    public function getOneUserAction($id)
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'users/'.$id;
        $client = new Client([
            'base_uri' => $api_uri
        ]);
        $response = $client->request('GET', $uri);
        $content = $response->getBody()->getContents();
        $user = json_decode($content, true);

        return $this->json($user);
    }


    /**
     * @Route("/userpost", name="userpost")
     *
     * Create new user
     */
    public function postUserAction()
    {
        $api_uri = $this->getContainer()->getParameter('api_uri');
        $uri = 'users';
        $data = [ 'json' => [
                    "name" => "qwerty",
                    'email' => 'qweabc@mail.com',
                    'role' => 1
                ]];
        $client = new Client([ 'base_uri' => $api_uri ]);
        $response = $client->request('POST', $uri, $data);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);

        return $this->json($message);
    }


    /**
     * @Route("/userput/{id}", name="userput")
     *
     * Edit user
     */
    public function editOneUserAction($id)
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'users/'.$id;
        $data = [ 'json' => [
            "name" => "Aqwerty",
            'email' => 'Aqweabc@mail.com',
        ]];
        $client = new Client([ 'base_uri' => $api_uri ]);
        $response = $client->request('PUT', $uri, $data);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);

        return $this->json($message);
    }

    /**
     * @Route("/userdel/{id}", name="userdel")
     *
     * Remove user
     */
    public function deleteOneUserAction($id)
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'users/'.$id;
        $client = new Client(['base_uri' => $api_uri]);
        $response = $client->request('DELETE', $uri);
        $content = $response->getBody()->getContents();
        $message = json_decode($content, true);

        return $this->json($message);
    }

    /**
     * @Route("/users/roles/{id}", name="usersroles")
     *
     * Get users list by role_id
     */
    public function getUsersRoleListAction($id)
    {
        $api_uri = $this->container->getParameter('api_uri');
        $uri = 'users/roles/'.$id;
        $client = new Client([ 'base_uri' => $api_uri ]);
        $response = $client->request('GET', $uri);
        $content = $response->getBody()->getContents();
        $list = json_decode($content, true);

        return $this->json($list);
    }
}
